﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace BibliotekaPO
{
   public class Książka
    {
        int idKsiążki;
        string tytułKsiążki;
        string autorKsiążki;
        string gatunekKsiążki;

        public int IdKsiążki { get => idKsiążki; set => idKsiążki = value; }
        public string TytułKsiążki { get => tytułKsiążki; set => tytułKsiążki = value; }
        public string AutorKsiążki { get => autorKsiążki; set => autorKsiążki = value; }
        public string GatunekKsiążki { get => gatunekKsiążki; set => gatunekKsiążki = value; }
        public Książka(int idKsiążki, string tytułKsiążki, string autorKsiążki, string gatunekKsiążki)
        {
            this.IdKsiążki = idKsiążki;
            this.TytułKsiążki = tytułKsiążki;
            this.AutorKsiążki = autorKsiążki;
            this.GatunekKsiążki = gatunekKsiążki;
        }
        public override string ToString()
        {
            return "ID: " + idKsiążki + ",  " + tytułKsiążki + ", " + autorKsiążki + ", " + gatunekKsiążki;
        }


            
    }
}
