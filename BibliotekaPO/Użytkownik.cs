﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaPO
{
    
    public class Użytkownik
    {
        string imie;
        string nazwisko;
        string telefon;
       

        public string Imie { get => imie; set => imie = value; }
        public string Nazwisko { get => nazwisko; set => nazwisko = value; }
        public string Telefon { get => telefon; set => telefon = value; }
     
        public Użytkownik(string imie, string nazwisko, string telefon)
        {
            this.Imie = imie;
            this.Nazwisko = nazwisko;
            this.Telefon = telefon;
          
        }

        public override string ToString()
        {
            return Imie + ", " + Nazwisko + ", " + Telefon;
        }



    }


}
