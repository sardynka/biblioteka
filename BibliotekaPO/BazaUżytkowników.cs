﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotekaPO
{
    public class BazaUżytkowników
    {
        public List<Użytkownik> użytkownicy = new List<Użytkownik>();
        public void DodajUżytkownika(Użytkownik u)
        {
            użytkownicy.Add(u);
        }
        public void UsuńUżytkownika(Użytkownik u)
        {
            użytkownicy.Remove(u);
        }

      
    }
}
