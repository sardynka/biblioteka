﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BibliotekaPO;
using System.Collections.ObjectModel;

namespace WPF
{
    /// <summary>
    /// Logika interakcji dla klasy Użytkownicy.xaml
    /// </summary>
    public partial class Użytkownicy : Window
    {
        BazaUżytkowników użytkownicy = new BazaUżytkowników();
        ObservableCollection<Użytkownik> listaUżytkowników;
        public Użytkownicy()
        {
            InitializeComponent();
        }

        private void Dodaj_Click(object sender, RoutedEventArgs e)
        {
            Użytkownik u = new Użytkownik(TextBox_I.Text, TextBox_N.Text, TextBox_T.Text);
            użytkownicy.DodajUżytkownika(u);
            listaUżytkowników = new ObservableCollection<Użytkownik>(użytkownicy.użytkownicy);
            List_U.ItemsSource = listaUżytkowników;


        }

        private void List_U_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow noweOkno = new MainWindow();
            noweOkno.Show(); //otwieranie nowego okna
            this.Close();
        }
    }
}
