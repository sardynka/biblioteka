﻿using BibliotekaPO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

using System.Collections.ObjectModel;

namespace WPF
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Książka> listaKsiążek = new List<Książka>();




        ObservableCollection<Książka> ksiazki;
        public MainWindow()
        {
            InitializeComponent();

            

            string connection = "server=localhost;port=3306;uid=root;pwd=;database=biblioteka;SslMode=none;";
            MySqlConnection con = new MySqlConnection(connection);
            con.Open(); // otwieranie połączenia z bazą danych MySQL


            /// dodawanie do Listy
            MySqlCommand cmd1 = new MySqlCommand("SELECT * FROM ksiazki ", con); //zaptanie SQL, pobieranie wszystkich rekordów/książek z tabeli 

            MySqlDataReader reader = cmd1.ExecuteReader();

            while (reader.Read())
            {
                var ks = new Książka(reader.GetInt32("id"), reader.GetString("nazwa"), reader.GetString("autor"), reader.GetString("gatunek"));
                listaKsiążek.Add(ks);

            }


            ksiazki = new ObservableCollection<Książka>(listaKsiążek);
            ListBox_ksiazki.ItemsSource = ksiazki;

            con.Close();

    



        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string connection = "server=localhost;port=3306;uid=root;pwd=;database=biblioteka;SslMode=none;";
            MySqlConnection con = new MySqlConnection(connection);
            con.Open(); // otwieranie połączenia z bazą danych MySQL
            MySqlCommand cmd2 = new MySqlCommand("INSERT INTO ksiazki (nazwa,autor,gatunek) VALUES(?,?,?)", con);  //zapytanie SQL, dodawanie ksiazek do bazy danych
            cmd2.Parameters.Add("?nazwa", MySqlDbType.VarChar).Value = TextBox_T.Text; //zapobieganie SQL Injection
            cmd2.Parameters.Add("?autor", MySqlDbType.VarChar).Value = TextBox_A.Text; //zapobieganie SQL Injection
            cmd2.Parameters.Add("?gatunek", MySqlDbType.VarChar).Value = TextBox_G.Text; //zapobieganie SQL Injection
            cmd2.ExecuteNonQuery();

            long id = cmd2.LastInsertedId; // pobieranie ostatnio wczytywanego ID do bazy 


            ksiazki.Add(new Książka((int)id, TextBox_T.Text, TextBox_A.Text, TextBox_G.Text)); // dodawanie Ksiazki do ListBoxa

            con.Close();


        }

        private void ListBox_ksiazki_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Użytkownicy noweOkno = new Użytkownicy();
            noweOkno.Show(); //otwieranie nowego okna
            this.Close();
        }
    }
}
