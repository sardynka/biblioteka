﻿namespace GUI
{
    partial class DodajKsiazke
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ksiazka = new System.Windows.Forms.Label();
            this.Tytul = new System.Windows.Forms.Label();
            this.Autor = new System.Windows.Forms.Label();
            this.Gatunek = new System.Windows.Forms.Label();
            this.TytulText = new System.Windows.Forms.TextBox();
            this.AutorText = new System.Windows.Forms.TextBox();
            this.GatunekText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Ksiazka
            // 
            this.Ksiazka.AutoSize = true;
            this.Ksiazka.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Ksiazka.Location = new System.Drawing.Point(197, 9);
            this.Ksiazka.Name = "Ksiazka";
            this.Ksiazka.Size = new System.Drawing.Size(197, 31);
            this.Ksiazka.TabIndex = 0;
            this.Ksiazka.Text = "Dodaj książkę";
            // 
            // Tytul
            // 
            this.Tytul.AutoSize = true;
            this.Tytul.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Tytul.Location = new System.Drawing.Point(31, 80);
            this.Tytul.Name = "Tytul";
            this.Tytul.Size = new System.Drawing.Size(120, 25);
            this.Tytul.TabIndex = 1;
            this.Tytul.Text = "Dodaj tytuł ";
            // 
            // Autor
            // 
            this.Autor.AutoSize = true;
            this.Autor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Autor.Location = new System.Drawing.Point(31, 154);
            this.Autor.Name = "Autor";
            this.Autor.Size = new System.Drawing.Size(135, 25);
            this.Autor.TabIndex = 2;
            this.Autor.Text = "Dodaj autora";
            // 
            // Gatunek
            // 
            this.Gatunek.AutoSize = true;
            this.Gatunek.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Gatunek.Location = new System.Drawing.Point(31, 226);
            this.Gatunek.Name = "Gatunek";
            this.Gatunek.Size = new System.Drawing.Size(151, 25);
            this.Gatunek.TabIndex = 3;
            this.Gatunek.Text = "Dodaj gatunek";
            this.Gatunek.Click += new System.EventHandler(this.label4_Click);
            // 
            // TytulText
            // 
            this.TytulText.Location = new System.Drawing.Point(256, 80);
            this.TytulText.Name = "TytulText";
            this.TytulText.Size = new System.Drawing.Size(166, 20);
            this.TytulText.TabIndex = 4;
            this.TytulText.TextChanged += new System.EventHandler(this.TytulText_TextChanged);
            // 
            // AutorText
            // 
            this.AutorText.Location = new System.Drawing.Point(256, 154);
            this.AutorText.Name = "AutorText";
            this.AutorText.Size = new System.Drawing.Size(166, 20);
            this.AutorText.TabIndex = 5;
            this.AutorText.TextChanged += new System.EventHandler(this.AutorText_TextChanged);
            // 
            // GatunekText
            // 
            this.GatunekText.Location = new System.Drawing.Point(256, 232);
            this.GatunekText.Name = "GatunekText";
            this.GatunekText.Size = new System.Drawing.Size(166, 20);
            this.GatunekText.TabIndex = 6;
            this.GatunekText.TextChanged += new System.EventHandler(this.GatunekText_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(256, 299);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // DodajKsiazke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 354);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.GatunekText);
            this.Controls.Add(this.AutorText);
            this.Controls.Add(this.TytulText);
            this.Controls.Add(this.Gatunek);
            this.Controls.Add(this.Autor);
            this.Controls.Add(this.Tytul);
            this.Controls.Add(this.Ksiazka);
            this.Name = "DodajKsiazke";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Ksiazka;
        private System.Windows.Forms.Label Tytul;
        private System.Windows.Forms.Label Autor;
        private System.Windows.Forms.Label Gatunek;
        private System.Windows.Forms.TextBox TytulText;
        private System.Windows.Forms.TextBox AutorText;
        private System.Windows.Forms.TextBox GatunekText;
        private System.Windows.Forms.Button button1;
    }
}

