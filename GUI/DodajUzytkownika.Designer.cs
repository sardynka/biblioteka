﻿namespace GUI
{
    partial class DodajUzytkownika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Uzytkownik = new System.Windows.Forms.Label();
            this.Imie = new System.Windows.Forms.Label();
            this.Nazwisko = new System.Windows.Forms.Label();
            this.Plec = new System.Windows.Forms.Label();
            this.Telefon = new System.Windows.Forms.Label();
            this.TelefonText = new System.Windows.Forms.TextBox();
            this.NazwiskoText = new System.Windows.Forms.TextBox();
            this.ImieText = new System.Windows.Forms.TextBox();
            this.PlecList = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Uzytkownik
            // 
            this.Uzytkownik.AutoSize = true;
            this.Uzytkownik.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Uzytkownik.Location = new System.Drawing.Point(141, 22);
            this.Uzytkownik.Name = "Uzytkownik";
            this.Uzytkownik.Size = new System.Drawing.Size(257, 25);
            this.Uzytkownik.TabIndex = 0;
            this.Uzytkownik.Text = "Wprowadź użytkownika";
            this.Uzytkownik.Click += new System.EventHandler(this.label1_Click);
            // 
            // Imie
            // 
            this.Imie.AutoSize = true;
            this.Imie.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Imie.Location = new System.Drawing.Point(44, 82);
            this.Imie.Name = "Imie";
            this.Imie.Size = new System.Drawing.Size(98, 24);
            this.Imie.TabIndex = 1;
            this.Imie.Text = "Podaj imię";
            // 
            // Nazwisko
            // 
            this.Nazwisko.AutoSize = true;
            this.Nazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Nazwisko.Location = new System.Drawing.Point(44, 128);
            this.Nazwisko.Name = "Nazwisko";
            this.Nazwisko.Size = new System.Drawing.Size(140, 24);
            this.Nazwisko.TabIndex = 2;
            this.Nazwisko.Text = "Podaj nazwisko";
            // 
            // Plec
            // 
            this.Plec.AutoSize = true;
            this.Plec.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Plec.Location = new System.Drawing.Point(44, 172);
            this.Plec.Name = "Plec";
            this.Plec.Size = new System.Drawing.Size(123, 24);
            this.Plec.TabIndex = 3;
            this.Plec.Text = "Wybierz płeć";
            // 
            // Telefon
            // 
            this.Telefon.AutoSize = true;
            this.Telefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Telefon.Location = new System.Drawing.Point(44, 224);
            this.Telefon.Name = "Telefon";
            this.Telefon.Size = new System.Drawing.Size(152, 24);
            this.Telefon.TabIndex = 4;
            this.Telefon.Text = "Podaj nr telefonu";
            // 
            // TelefonText
            // 
            this.TelefonText.Location = new System.Drawing.Point(249, 224);
            this.TelefonText.Name = "TelefonText";
            this.TelefonText.Size = new System.Drawing.Size(149, 20);
            this.TelefonText.TabIndex = 5;
            this.TelefonText.TextChanged += new System.EventHandler(this.TelefonText_TextChanged);
            // 
            // NazwiskoText
            // 
            this.NazwiskoText.Location = new System.Drawing.Point(249, 128);
            this.NazwiskoText.Name = "NazwiskoText";
            this.NazwiskoText.Size = new System.Drawing.Size(149, 20);
            this.NazwiskoText.TabIndex = 6;
            this.NazwiskoText.TextChanged += new System.EventHandler(this.NazwiskoText_TextChanged);
            // 
            // ImieText
            // 
            this.ImieText.Location = new System.Drawing.Point(249, 82);
            this.ImieText.Name = "ImieText";
            this.ImieText.Size = new System.Drawing.Size(149, 20);
            this.ImieText.TabIndex = 7;
            this.ImieText.TextChanged += new System.EventHandler(this.ImieText_TextChanged);
            // 
            // PlecList
            // 
            this.PlecList.FormattingEnabled = true;
            this.PlecList.Items.AddRange(new object[] {
            "K",
            "M"});
            this.PlecList.Location = new System.Drawing.Point(249, 172);
            this.PlecList.Name = "PlecList";
            this.PlecList.Size = new System.Drawing.Size(149, 30);
            this.PlecList.TabIndex = 8;
            this.PlecList.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 287);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // DodajUzytkownika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 344);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.PlecList);
            this.Controls.Add(this.ImieText);
            this.Controls.Add(this.NazwiskoText);
            this.Controls.Add(this.TelefonText);
            this.Controls.Add(this.Telefon);
            this.Controls.Add(this.Plec);
            this.Controls.Add(this.Nazwisko);
            this.Controls.Add(this.Imie);
            this.Controls.Add(this.Uzytkownik);
            this.Name = "DodajUzytkownika";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.DodajUzytkownika_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Uzytkownik;
        private System.Windows.Forms.Label Imie;
        private System.Windows.Forms.Label Nazwisko;
        private System.Windows.Forms.Label Plec;
        private System.Windows.Forms.Label Telefon;
        private System.Windows.Forms.TextBox TelefonText;
        private System.Windows.Forms.TextBox NazwiskoText;
        private System.Windows.Forms.TextBox ImieText;
        private System.Windows.Forms.ListBox PlecList;
        private System.Windows.Forms.Button button1;
    }
}